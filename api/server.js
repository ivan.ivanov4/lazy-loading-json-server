// JSON Server module
const jsonServer = require("json-server");
const auth = require('json-server-auth')
const server = jsonServer.create();
const router = jsonServer.router("data.json");

server.db = router.db

// Make sure to use the default middleware
const middlewares = jsonServer.defaults();

server.use(middlewares);
// Add this before server.use(router)
server.use(
 // Add custom route here if needed
 jsonServer.rewriter({
    "/vrscans*": "/640/vrscans$1",
    "/materials*": "/640/materials$1",
    "/colors*": "/640/colors$1",
    "/tags*": "/640/tags$1",

    "/industries*": "/640/industries$1",
    "/manufacturers*": "/640/manufacturers$1",
    
    "/favorites*": "/660/favorites$1"
})
);
server.use(auth);
server.use(router);
// Listen to port
server.listen(3000, () => {
 console.log("JSON Server is running");
});

// Export the Server API
module.exports = server;